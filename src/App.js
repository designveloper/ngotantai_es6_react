import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
// import Clock from './Components/Clock';
import ProductPage from './Components/ProductPage';

class App extends Component {
  render() {
    return (
      <div>
       {/* <Clock /> */}
       <ProductPage/>
      </div>
    );
  }
}

export default App;
